<?php
require 'swiftmailer/vendor/autoload.php';
require_once('swiftmailer/lib/swift_required.php');

$transport = (new Swift_SmtpTransport('smtp.mailtrap.io', 2525, 'tls'))
    ->setUserName('***************')
    ->setPassword('***************');
$mailer = new Swift_Mailer($transport);

$message = (new Swift_Message('Weekly Hours'))
   ->setFrom (array('email@gmail.com' => 'My Name'))
   ->setTo (array('email@hotmail.com' => 'Recipient'))
   ->setSubject ('Weekly Hours')
   ->setBody ('Test Message', 'text/html');

$sendemail = $mailer->send($message);

// that's it
